#!/usr/bin/env python3

from random import random

"""
Rubbish Python code being used to demonstrate
branching & merging during the Git More session...
"""


def get_date():
    """Get today's date"""
    return 'Thursday 22nd November 2018'


def run_simulation(input):
    """Complicated simulation code goes here"""
    return input / random()


# Simulation work starts here
print('Today is {}'.format(get_date()))
input = 100
result = run_simulation(input)
print('Result is {}'.format(result))
